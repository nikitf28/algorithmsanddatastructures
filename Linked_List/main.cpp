#include <iostream>
#include <algorithm>
#include <numeric>

using namespace std;

class Node{
public:
    int data;
    Node* next;

    Node(): data(0){
        next = nullptr;
    }

    Node(int data): data(data){
        next = nullptr;
    }

    Node(int array[], int n){
        Node *node = this;
        node->data = array[0];
        for (int i = 1; i < n; i++){
            node->next = new Node(array[i]);
            node = node->next;
        }
    }

    Node(initializer_list<int> list){
        if (list.size() != 0){
            Node* node = this;
            auto it = list.begin();
            node->data = *it;
            ++it;
            for (; it != list.end(); ++it){
                node->next = new Node(*it);
                node = node->next;
            }
        }
    }

    int count(){
        int c = 0;
        Node* node = this;
        while (node){
            c++;
            node = node->next;
        }
        return c;
    }
    int sum(){
        int c = 0;
        Node* node = this;
        while (node){
            c+= node->data;
            node = node->next;
        }
        return c;
    }
    int max(){
        Node* node = this;
        int c = 0;
        while (node){
            if (node->data > c){
                c = node->data;
            }
            node = node->next;
        }
        return c;
    }

    int rmax(){
        return rmax(this, INT32_MIN);
    }

    void display(){
        display(this);
        cout <<endl;
    }

private:
    int rmax(Node *node, int c){
        c = ::max(c, node->data);
        if (node->next != nullptr){
            return rmax(node->next, c);
        }

        else{
            return c;
        }
    }

    int rmax(Node *p){
        int max = 0;
        if (p == nullptr){
            return INT32_MIN;
        }

        max = rmax(p->next);
        if (max > p->data){
            return max;
        }
        return p->data;
    }

    void display(Node *p){
        cout <<p->data <<" -> " <<flush;
        if (p->next != nullptr){
            display(p->next);
        }
    }
};

class LinkedList{
public:
    LinkedList(){
        first = nullptr;
    }
    LinkedList(initializer_list<int> list){
        first = new Node(list);
    }
    void display(){
        first->display();
    }

    class Iterator{
    public:
        Iterator():current(nullptr){};
        Iterator(const Node* node): current(node) {};

        int operator*(){
            //cout <<"call *operator" <<endl;
            return current->data;
        }
        Iterator &operator++(){
            //cout <<"call ++operator" <<endl;
            if (current != nullptr){
                previous = current;
                current = current->next;
            }
            return *this;
        }
        Iterator operator++(int){
            //cout <<"call ++(int)operator" <<endl;
            Iterator temp = *this;
            ++*this;
        }

        bool operator!=(const Iterator &other){
            //cout <<"call !=operator" <<endl;
            return this->current != other.current;
        }
    private:
        const Node *previous = nullptr;
        const Node *current = nullptr;
    };

    Iterator begin() const noexcept{
        return Iterator(first);
    }
    Iterator end() const noexcept{
        return Iterator();
    }

private:
    Node *first;
};

int main() {

    int A[] = {3, 5, 7, 10, 15, 8, 12, 20};
    Node *head = new Node(A, 8);
    head->display();

    LinkedList list = {3, 5, 7, 10, 15, 8, 12, 20};
    list.display();
    for (auto bla : list){
        cout <<bla <<endl;
    }

    int sum = 0;
    for_each(list.begin(), list.end(), [&] (int n){
       sum += n;
    });
    cout <<sum <<endl;
    int accum = accumulate(list.begin(), list.end(), 0);
    cout <<accum;
    return 0;
}