import os
import random
import time


def hash_CRC(string):
    h = 0
    for symbol in string:
        highorder = h & 0xF8000000
        h = (h << 5) & 0xFFFFFFFF
        h = h ^ (highorder >> 27)
        h = h ^ ord(symbol)
    return h


def hash_PJW(string):
    h = 0
    for symbol in string:
        h = ((h << 4) + ord(symbol)) & 0xFFFFFFFF
        g = h & 0xf0000000
        if g != 0:
            h = h ^ (g >> 24)
            h = h ^ g
    return h


def hash_BUZ(string):
    global R
    h = 0
    for symbol in string:
        highorder = h & 0x80000000
        h = (h << 1) & 0xFFFFFFFF
        h = h ^ (highorder >> 31)
        h = h ^ R[ord(symbol)]
    return h


def generate_R():
    R = {}
    nums = [i for i in range(10 ** 5)]
    for i in range(256):
        random_index = random.randint(0, len(nums) - 1)
        R[i] = nums[random_index]
        nums.pop(random_index)
    return R


def remove_characters(string):
    characters = [' ', '\n']
    new_string = ''
    for symbol in string:
        if symbol not in characters:
            new_string += symbol
    return new_string


def get_texts(folder):
    txts = []
    files = os.listdir(folder)
    for file in files:
        f = open(folder + '/' + file)
        text = remove_characters(f.read())
        txts.append(text)
    return txts


def find_duplicates(txts, hash_function):
    time_start = time.perf_counter_ns()
    hashes = set(map(hash_function, txts))
    time_elapsed = time.perf_counter_ns() - time_start
    print('Function: %s' % hash_function.__name__)
    print('Time spent: %s' % time_elapsed)
    print('Original files: %s' % len(hashes))
    print('Duplicates: %s' % (len(txts) - len(hashes)))
    print('========================================')


R = generate_R()
texts = get_texts('files')
find_duplicates(texts, hash_CRC)
find_duplicates(texts, hash_PJW)
find_duplicates(texts, hash_BUZ)
find_duplicates(texts, hash)
