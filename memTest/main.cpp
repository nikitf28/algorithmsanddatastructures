#include <iostream>
#include <vector>
#include <map>
#include <cmath>

using namespace std;

int main() {
    map<int, int> my_map;
    int s;
    for (int i = 0; i <= 8; i++){
        int start_time = clock();
        s = pow(10, i);
        my_map.clear();
        for (int j = 0; j < s; j++){
            my_map.insert(make_pair(INT32_MAX, INT32_MAX));
        }
        int end_time = clock();
        cout <<s <<" " <<sizeof (map<int, int>) + sizeof(make_pair(INT32_MAX, INT32_MAX)) * s
        <<" " <<(long long)(double(end_time - start_time) / CLOCKS_PER_SEC * 1000000000L) <<endl;
    }
    return 0;
}
