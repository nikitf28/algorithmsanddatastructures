import sys
from matplotlib import pyplot as plt

cpp_n = []
cpp_sizes = []
cpp_time = []
cpp_file = open(sys.argv[1])
for line in cpp_file:
    splitted = line.split()
    cpp_n.append(int(splitted[0]))
    cpp_sizes.append(int(splitted[1]))
    cpp_time.append(int(splitted[2]))


py_n = []
py_sizes = []
py_time = []
py_file = open(sys.argv[2])
for line in py_file:
    splitted = line.split()
    py_n.append(int(splitted[0]))
    py_sizes.append(int(splitted[1]))
    py_time.append(int(splitted[2]))


ax = plt.subplot(1, 2, 1)
plt.plot(cpp_n, cpp_sizes, label='C++')
plt.plot(py_n, py_sizes, label='Python')
plt.title('Зависимости размера ассоциавтивного массива\nот колличества элементов')
plt.grid()
plt.legend()
ax.set_xscale('log')
ax.set_yscale('log')
plt.xlabel('Количетсво элементов стуруктуры')
plt.ylabel('Размер структуры, байты')

ax = plt.subplot(1, 2, 2)
plt.plot(cpp_n, cpp_time, label='C++')
plt.plot(py_n, py_time, label='Python')
plt.title('Зависимость времени заполения ассоциавтивногомассива\nот колличества элементов')
plt.grid()
plt.legend()
ax.set_xscale('log')
ax.set_yscale('log')
plt.xlabel('Количетсво элементов стуруктуры')
plt.ylabel('Время выполнения, наносекунды')

plt.show()
