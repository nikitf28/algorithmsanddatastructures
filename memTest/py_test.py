import sys
import time

for i in range(0, 8):
    num = 10 ** i
    mp = {}
    time_start = time.perf_counter_ns()
    for j in range(num):
        mp[j] = j
    time_end = time.perf_counter_ns()
    print(num, sys.getsizeof(mp), time_end - time_start)
