#include <iostream>

using namespace std;

char do_hash(string str){
    short* array = new short[8];
    for (char sym : str){
        for (short i = 0; i < 8; i++){
            array[i] = (array[i] + (sym >> i) % 2) % 2;
        }
    }
    char result = 0;
    for (short i = 7; i >= 0; i--){
        result = (result << 1) + (array[i] % 2);
    }
    return result;
}

void print_binary(char sym){
    for (short i = 7; i >= 0; i--){
        cout <<(sym >> i) % 2;
    }
    cout <<endl;
}

int main() {
    print_binary(do_hash("asdqwerty"));
    print_binary(do_hash("qwertyasd"));
}
