import matplotlib.pyplot as plt
from typing import Tuple
import math
import time

def distance(lat1, lon1, lat2, lon2):
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    lon1 = math.radians(lon1)
    lon2 = math.radians(lon2)

    r = 6371
    a = math.sin((lat2 - lat1) / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin((lon2 - lon1) / 2) ** 2
    d = 2 * r * math.asin(a ** 0.5)
    return d


def get_roi(lats, lons, vals, roi):
    result = []
    for lat, lon, val in zip(lats, lons, vals):
        if roi[0] < lon < roi[1] and roi[2] < lat > roi[3]:
            result.append([lat, lon, val])
    return result


def thin_grid(points, min_distance=1000):
    result = []
    p = points
    result.append(p)
    while points:
        p2 = points.pop()
        for p1 in points:
            if distance(*p1[:2], *p2[:2]) < min_distance:
                break
        else:
            result.append(p2)
    return result


def memorize(func):
    cache = dict()
    def wrapped(*args, **kwargs):
        print(f"Called at {time.time()}")
        param_hash = hash((args, tuple(kwargs.items())))
        if param_hash in cache:
            print("Get result from hash")
            return cache[param_hash]
        result = func(*args, **kwargs)
        cache[param_hash] = result
        return result
    return wrapped


@memorize
def query(date: str, roi: Tuple[float], min_distance: int = 1000):
    with open("data/" + date + ".dat") as f:
        lats = []
        lons = []
        vals = []
        for line in f.readlines()[1:]:
            lat, lon, val = list(map(float, line.split()))
            lats.append(lat)
            lons.append(lon)
            vals.append(val)

    roi = get_roi(lats, lons, vals, roi)
    result = thin_grid(roi, min_distance)

t = time.perf_counter()
result = query("2020-01-01", (-100, -80, 28, 45))
print(f"Elapsed: {time.perf_counter() - t:.5}")

t = time.perf_counter()
result = query("2020-01-01", (-100, -80, 28, 45))
print(f"Elapsed: {time.perf_counter() - t:.5f}")