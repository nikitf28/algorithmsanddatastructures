with open("out.txt", "r") as f:
    lines = f.readlines()
    n = len(lines)
    lines = map(lambda item: list(map(int, item.split())), lines)
    bit_count = lambda item: bin(item).count("1")
    odd_bit = lambda items: sum(map(bit_count, items[:4])) % 2 == items[-1]
    result = map(odd_bit, lines)
    print((1 - sum(list(result)) / n) * 100.)