from itertools import permutations

key = "asd"


def hash(key):
    result = ord(key[0])
    for symb in key[1:]:
        result = result or ord(symb)
    return result


print(bin(hash(key)))

hashes = []
for perm in permutations(key, len(key)):
    s = "".join(perm)
    h = hash(s)
    hashes.append(h)
    print(f"{s} - {h}")

print(hashes)
print(len(hashes) - len(set(hashes)))
