//
// Created by nikita on 03.04.2022.
//

#ifndef MATRIX_NDARRAY_SHAPE_H
#define MATRIX_NDARRAY_SHAPE_H
using namespace std;
#include <iostream>

class NDArray_shape {
private:
    int dimensions;
    int* shapes;
public:
    bool operator==(const NDArray_shape &rhs) const;

    bool operator!=(const NDArray_shape &rhs) const;

    NDArray_shape(int dimensions, int* shapes);
    NDArray_shape();

    int get_dimesions() const;
    int get_shape(int x) const;
    int get_total_cells() const;
};


#endif //MATRIX_NDARRAY_SHAPE_H
