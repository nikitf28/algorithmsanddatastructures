#include <iostream>
#include "NDArray.h"

using namespace std;


int main() {
    NDArray ndArray(new int [9]{1, 2, 3, 4, 5, 6, 7, 8, 9}, NDArray_shape(2, new int[2]{3, 3}));
    NDArray ndArray1(new int [9]{1, 2, 3, 4, 5, 6}, NDArray_shape(2, new int[2]{3, 2}));

    cout <<ndArray <<endl;
    cout <<ndArray.avg(0, 0, 1, 1);
}
