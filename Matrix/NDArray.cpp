//
// Created by nikita on 03.04.2022.
//

#include "NDArray.h"
#include "random"

NDArray::NDArray(int fill, NDArray_shape shape) : NDArray(shape) {
    for (int i = 0; i < get_total_cells(); i++){
        array[i] = fill;
    }
}

NDArray::NDArray(int *fill, NDArray_shape shape) : NDArray(shape) {
    for (int i = 0; i < get_total_cells(); i++){
        array[i] = fill[i];
    }
}

NDArray::NDArray(NDArray_shape shape) {
    this->shape = shape;
    array = new int[get_total_cells()];
}

int NDArray::ndim() const {
    return shape.get_dimesions();
}

int NDArray::get_total_cells() const{
    return shape.get_total_cells();
}

void NDArray::randomise(int min_value, int max_value) {
    for (int i = 0; i < get_total_cells(); i++){
        array[i] = rand() % (max_value - min_value) + min_value;
    }
}

void NDArray::randomise(int max_value) {
    randomise(0, max_value);
}

void NDArray::randomise() {
    randomise(INT32_MIN/2, INT32_MAX/2);
}

std::ostream &operator<<(std::ostream &os, const NDArray &array) {
    if (array.ndim() == 1){
        for (int i = 0; i < array.get_shape(0); i++){
            os <<array.array[i] <<" ";
        }
    }
    else if (array.ndim() == 2){
        for (int i = 0; i < array.get_shape(0); i++){
            for (int j = 0; j < array.get_shape(1); j++){
                os <<array.get(i, j) <<" ";
            }
            os <<std::endl;
        }
    }
    return os;
}

int NDArray::get_shape(int x) const{
    return shape.get_shape(x);
}

int NDArray::get(int x, int y) const {
    if (ndim() < 2){
        throw NDArray_Exception();
    }
    return array[get_shape(1)*x + y];
}

int NDArray::get(int x) const {
    return array[x];
}

NDArray operator+(const NDArray &n1, const NDArray &n2) {
    return n1.do_math(n1, n2, 1);
}

NDArray operator-(const NDArray &n1, const NDArray &n2) {
    return n1.do_math(n1, n2, 2);
}

NDArray operator*(const NDArray &n1, const NDArray &n2) {
    return n1.do_math(n1, n2, 3);
}

NDArray operator/(const NDArray &n1, const NDArray &n2) {
    return n1.do_math(n1, n2, 4);
}

NDArray NDArray::do_math(const NDArray &n1, const NDArray &n2, int c) const {
    if (n1.shape != n2.shape){
        throw NDArray_Exception();
    }
    NDArray new_nd(n1.shape);
    for (int i = 0; i < n1.get_total_cells(); i++){
        if (c == 1){
            new_nd.array[i] = n1.array[i] + n2.array[i];
        }
        if (c == 2){
            new_nd.array[i] = n1.array[i] - n2.array[i];
        }
        if (c == 3){
            new_nd.array[i] = n1.array[i] * n2.array[i];
        }
        if (c == 4){
            new_nd.array[i] = n1.array[i] / n2.array[i];
        }

    }
    return new_nd;
}

NDArray NDArray::T() const {
    if (ndim() != 2){
        throw NDArray_Exception();
    }

    NDArray tMatrix(NDArray_shape(2, new int[2]{get_shape(1), get_shape(0)}));
    for (int i = 0; i < get_shape(0); i++){
        for (int j = 0; j < get_shape(1); j++){
            tMatrix.set(j, i, get(i, j));
        }
    }
    return tMatrix;
}

void NDArray::set(int x, int value) {
    array[x] = value;
}

void NDArray::set(int x, int y, int value) {
    array[get_shape(1)*x + y] = value;
}

NDArray operator%(const NDArray &n1, const NDArray &n2) {
    if (n1.get_dimesions() != n2.get_dimesions()){
        throw NDArray_Exception();
    }
    if (n1.get_shape(1) != n2.get_shape(0)){
        throw NDArray_Exception();
    }
    NDArray mulledMatrix = NDArray(NDArray_shape(2, new int[2]{n1.get_shape(0), n2.get_shape(1)}));

    for (int i = 0; i < n1.get_shape(0); i++){
        for (int j = 0; j < n2.get_shape(1); j++){
            mulledMatrix.set(i, j, (n1.get_row(i) * n2.T().get_row(j)).sum());
        }
    }
    return mulledMatrix;
}

int NDArray::get_dimesions() const {
    return shape.get_dimesions();
}

NDArray NDArray::get_row(int x) const {
    int* new_row = new int[get_shape(1)];
    for (int i = 0; i < get_shape(1); i++){
        new_row[i] = get(x, i);
    }
    NDArray row(new_row, NDArray_shape(1, new int[1]{get_shape(1)}));
    return row;
}

int NDArray::sum() {
    int sum = 0;
    for (int i = 0; i < get_total_cells(); i++){
        sum += get(i);
    }
    return sum;
}

int NDArray::min(int x1, int y1, int x2, int y2) {
    int mi = INT32_MAX;
    for (int i = x1; i <= x2; i++){
        for (int j = y1; j <= y2; j++){
            mi = std::min(mi, get(i, j));
        }
    }
    return mi;
}

int NDArray::max(int x1, int y1, int x2, int y2) {
    int ma = INT32_MIN;
    for (int i = x1; i <= x2; i++){
        for (int j = y1; j <= y2; j++){
            ma = std::max(ma, get(i, j));
        }
    }
    return ma;
}

double NDArray::avg(int x1, int y1, int x2, int y2) {
    int su = 0;
    for (int i = x1; i <= x2; i++){
        for (int j = y1; j <= y2; j++){
            su += get(i, j);
        }
    }
    return su * 1.0 / ((x2 - x1 + 1) * (y2 - y1 + 1));
}

