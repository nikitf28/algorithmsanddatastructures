//
// Created by nikita on 03.04.2022.
//

#include "NDArray_shape.h"



NDArray_shape::NDArray_shape(int dimensions, int* shapes) {
    this->dimensions = dimensions;
    this->shapes = new int[this->dimensions];
    for (int i = 0; i < this->dimensions; i++){
        this->shapes[i] = shapes[i];
    }
}

int NDArray_shape::get_dimesions() const {
    return dimensions;
}

int NDArray_shape::get_shape(int x) const {
    return shapes[x];
}

NDArray_shape::NDArray_shape() {}

int NDArray_shape::get_total_cells() const {
    int sum = 1;
    for (int i = 0; i < dimensions; i++){
        sum *= shapes[i];
    }
    return sum;
}

bool NDArray_shape::operator==(const NDArray_shape &rhs) const {
    if (this->dimensions != rhs.dimensions){
        return false;
    }
    for (int i = 0; i < this->dimensions; i++){
        if (this->shapes[i] != rhs.shapes[i]){
            return false;
        }
    }
    return true;
}

bool NDArray_shape::operator!=(const NDArray_shape &rhs) const {
    return !(rhs == *this);
}
