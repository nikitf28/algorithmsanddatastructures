//
// Created by nikita on 03.04.2022.
//

#ifndef MATRIX_NDARRAY_H
#define MATRIX_NDARRAY_H

#include <ostream>
#include "NDArray_shape.h"


class NDArray_Exception{};

class NDArray {
private:
    int* array;
    NDArray_shape shape;

public:
    NDArray(NDArray_shape shape);
    NDArray(int fill, NDArray_shape shape);
    NDArray(int* fill, NDArray_shape shape);

    void randomise(int min_value, int max_value);
    void randomise(int max_value);
    void randomise();

    int get_total_cells() const;
    int get_shape(int x) const;
    int get_dimesions() const;
    int ndim() const;

    int get(int x) const;
    int get(int x, int y) const;
    void set(int x, int value);
    void set(int x, int y, int value);
    NDArray get_row(int x) const;

    friend std::ostream &operator<<(std::ostream &os, const NDArray &array);
    friend NDArray operator +(const NDArray& n1, const NDArray& n2);
    friend NDArray operator -(const NDArray& n1, const NDArray& n2);
    friend NDArray operator *(const NDArray& n1, const NDArray& n2);
    friend NDArray operator /(const NDArray& n1, const NDArray& n2);
    friend NDArray operator %(const NDArray& n1, const NDArray& n2);

    NDArray do_math(const NDArray &n1, const NDArray &n2, int c) const;

    int sum();
    int min(int x1, int y1, int x2, int y2);
    int max(int x1, int y1, int x2, int y2);
    double avg(int x1, int y1, int x2, int y2);

    NDArray T() const;
};


#endif //MATRIX_NDARRAY_H
