import random
import matplotlib.pyplot as plt
import networkx as nx
from networkx.drawing.nx_pydot import graphviz_layout


class Node():
    node_id = 0

    def __init__(self):
        self._id = Node.node_id
        self.left = None
        self.right = None
        Node.node_id += 1

    def __str__(self):
        return f"Node({self._id})"


def gen_binary_tree(max_levels=3):
    root = Node()
    level = 0
    _gen_tree(root, level, max_levels)
    return root


def _gen_tree(parent, level, max_levels):
    level += 1
    if level >= max_levels:
        return
    if random.random() < (1 - level / max_levels) + 0.1:
        left = Node()
        parent.left = left
        _gen_tree(left, level, max_levels)
    if random.random() < (1 - level / max_levels) + 0.1:
        right = Node()
        parent.left = right
        _gen_tree(right, level, max_levels)


def count_nodes(node):
    if node is None:
        return 0
    return 1 + count_nodes(node.left) + count_nodes(node.right)


def preorder(node):
    if node is not None:
        yield node
        preorder(node.left)
        preorder(node.right)


def inorder(node):
    if node is not None:
        preorder(node.left)
        yield node
        preorder(node.right)


def postorder(node):
    if node is not None:
        preorder(node.left)
        preorder(node.right)
        yield node


root = Node()
root.left = Node()
root.right = Node()
root.left.left = Node()
root.left.right = Node()
root.right.left = Node()
root.right.right = Node()

print(preorder(root))
print(inorder(root))
print(postorder(root))
'''
tree = nx.Graph()
tree.add_nodes_from(nodes)
tree.add_edges_from(edges)

plt.figure(figsize=(10, 10))
pos = graphviz_layout(tree, prog="dot")
nx.draw(tree, pos, with_labels=True, font_size=22)
plt.show()
'''
