#include <iostream>
#include <string>
#include <filesystem>
#include <vector>
#include <fstream>

using namespace std;

vector<int> read_file(string path){
    ifstream in(path);
    string line;
    vector<int> result;
    while (getline(in, line)){
        result.push_back(stoi(line));
    }
    return result;
}

void search(string path, vector<int> &result){
    for (const auto &entry : filesystem::directory_iterator(path)){
        if (entry.is_regular_file()){
            vector<int> from_file = read_file(entry.path());
            for (int num : from_file){
                result.push_back(num);
            }
        }
        else{
            search(entry.path(), result);
        }
    }
}

int main()
{
    string path = "data";
    vector<int> result;
    search(path, result);
    //cout <<"SIZE: " <<result.size() <<endl;
    int sum = 0;
    for (int num : result){
        sum += num;
    }
    cout <<"SUM IS: " <<sum;
}