#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

int main(int argc, char *argv[]){
    int n = atoi(argv[1]);
    ofstream fout("data.txt");
    fout <<n <<endl;
    for (int i = 0; i < n; i++){
        fout <<(rand()*1./RAND_MAX)*M_PI*2 - M_PI <<" " <<rand()*1./RAND_MAX*M_PI*2 - M_PI <<" " <<rand()*1./RAND_MAX*M_PI*2 - M_PI <<endl;
    }
    fout.close();
}