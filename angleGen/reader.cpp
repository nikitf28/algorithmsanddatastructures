#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

int main(){
    int n;
    ifstream fin("data.txt");
    fin >>n;
    for (int i = 0; i < n; i++){
        double x, y, z;
        fin >>x >>y >>z;
        double angle = 2 * sin(x) * sin(y) + cos(z);
        cout <<angle * (180. / M_PI) <<endl;
    }
    fin.close();
}