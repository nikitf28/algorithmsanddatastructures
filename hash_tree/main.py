def closest_bin(n):
    k = 1
    while k < n:
        k *= 2
    return k


def do_hash(tree, i):
    if i >= len(tree) // 2:
        return tree[i]
    do_hash(tree, i * 2)
    do_hash(tree, i * 2 + 1)
    tree[i] = hash(tree[i * 2] + tree[i * 2 + 1])


def verify_hash(i):
    if i == 0:
        return True
    if tree[i] != hash(tree[i * 2] + tree[i * 2 + 1]):
        return False
    return verify_hash(i // 2)


def check_hash(tree, data, i):
    if hash(data) != tree[len(tree) // 2 + i]:
        return False
    return verify_hash(i // 2)


data = [10, 17, 18, 19, 20, 30, 43, 123, 1322, 13, 12]
tree_size = closest_bin(len(data) * 2)
tree = [0] * tree_size

for i in range(len(data)):
    tree[len(tree) // 2 + i] = hash(data[i])

do_hash(tree, 1)

print(check_hash(tree, 10, 0))